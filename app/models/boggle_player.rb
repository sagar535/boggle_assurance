class BogglePlayer < ApplicationRecord
  belongs_to :player, class_name: 'User', foreign_key: :player_id
  belongs_to :boggle

  has_many :boggle_words
  validates_uniqueness_of :player_id, scope: :boggle
end
