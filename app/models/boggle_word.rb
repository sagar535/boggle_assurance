class BoggleWord < ApplicationRecord
	belongs_to :boggle
	belongs_to :user, optional: true
	belongs_to :boggle_player, optional: true

	before_save :check_if_correct

	scope :correct, -> { where(correct: true) }
	scope :incorrect, -> { where(correct: [nil, false]) }


	validates :word, uniqueness: { scope: [:boggle, :user, :boggle_player] }

	private

	def check_if_correct
		unless boggle.combination_of_word_exists(word)
			self.correct = false
		else
			correct = Trie::WORD_TRIE.has_key?(word)

			self.correct = correct

			assign_score
		end
	end

	def assign_score
		self.score = word.size

		if correct?
			boggle_player.present? ?
					boggle_player.update_columns(score: (boggle_player.score + word.size)) :
					boggle.update_columns(score: (boggle.score + word.size))
		end
	end

end
