class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :organized_boggles, class_name: 'Boggle', foreign_key: :organizer_id
  has_many :boggle_players, foreign_key: :player_id
  has_many :boggles, through: :boggle_players
  has_many :boggle_words

  has_many :friendships, foreign_key: :follower_id
  has_many :followings, class_name: "User", through: :friendships, foreign_key: :follower_id

  has_many :subscribed_games, class_name: "Boggle", through: :followers


  def follower_ids
    Friendship.where(following_id: id).pluck(:follower_id)
  end

  def followers
    User.where(id: follower_ids)
  end

  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_initialize do |user|
      #user.user_name = auth.info.name
      user.email = auth.info.email
      user.password = SecureRandom.hex
    end
  end

  def following?(user)
    self.following_ids.include?(user.id)
  end

  def subscribed_games
    Boggle.where(organizer_id: following_ids)
  end
end
