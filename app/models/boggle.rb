class Boggle < ApplicationRecord

	has_many :boggle_words, dependent: :destroy
	belongs_to :user, optional: true
	belongs_to :organizer, class_name: "User", optional: true
	has_many :boggle_players, dependent: :destroy
	has_many :players, through: :boggle_players, class_name: "User", foreign_key: :player_id

	validates_length_of :letters, minimum: 16, maximum: 16
	before_validation :generate_random_16

	scope :on_games, -> { where(finished: false) }
	scope :unfinished, -> { on_games }
	scope :latest, -> { where('created_at > ?', 1.day.ago) }

	accepts_nested_attributes_for :boggle_words

	def generate_random_16
		self.letters = better_sequence if self.letters.blank?
	end

	def combination_of_word_exists(word)
		#represent letters in 4 * 4 grid
		grid = generate_grid

		# check to see if first letter in the word exists in grid
		return false unless letters.include? word[0]


		# will be extending the combinations of letter in grid to form the word
		indexes = find_index(grid, word[0])

		extend_path(word, indexes, [], word[0])[0]
	end

	def extend_path(word, indexes, path=[], prefix)
		return prefix, path if prefix == word

		(indexes-path).each do |i|
			path << i

			first_index = i
			second_letter = word[prefix.length]

			is_neighbour, neighbour_indexes = is_neighbour(first_index, second_letter, path)

			
			if is_neighbour
				prefix += second_letter
				nav, path = extend_path(word, neighbour_indexes, path, prefix)
			end

			return word, path if nav == word
		end

		return false, path
	end

	def is_neighbour(first_index, second_letter, path)
		grid = @grid || generate_grid

		neighbour_indexes = get_neighbours_index(first_index)

		
		neighbour = false
		indexes = []

		neighbour_indexes.each { |n_i|
			if grid[n_i[0]][n_i[1]] == second_letter && !path.include?([n_i[0], n_i[1]])
				neighbour = true
				indexes << n_i
			end
		}

		return neighbour, indexes
	end

	def get_neighbours_index(indexes)
		neighbour_indexes = []
		i, j = indexes

		neighbour_indexes << [i-1, j] if i > 0				#left
		neighbour_indexes << [i+1, j] if i < 3				#right

		neighbour_indexes << [i, j-1] if j > 0				#down
		neighbour_indexes << [i-1, j-1] if j > 0 && i > 0	#down-left
		neighbour_indexes << [i+1, j-1] if j > 0 && i < 3	#down-right


		neighbour_indexes << [i, j+1] if j < 3 				# up
		neighbour_indexes << [i-1, j+1] if j < 3 && i > 0	#up-left
		neighbour_indexes << [i+1, j+1] if j < 3 && i < 3	#up-right

		neighbour_indexes
	end

	def solve_boggle(words=[], 
						str='',
						i=0,
						j=0,
						visited=[[false, false, false, false], [false, false, false, false], [false, false, false, false], [false, false, false, false]])
		

		if str.empty? || (!visited[i][j] && Trie::WORD_TRIE.children(str).present?)
			grid = generate_grid
		
			p 'condition met'
			p i, j
			
			visited[i][j] = true
			# visited.map {|v| p v}
			neighbour_indexes = get_neighbours_index([i, j])
			str += grid[i][j]
			words << str if Trie::WORD_TRIE.has_key?(str)

			neighbour_indexes.select{|mn| visited[mn[0]][mn[1]] == false }.each do |ij|
				p str
				p ij
				visited.map {|v| p v}
				i, j = ij
				solve_boggle(words, str, i, j, visited)
				visited[i][j] = false		
			end
		end

		p words
		words.uniq
	end

	def solve_for_all_position
		words = []
		(0..3).each do |i|
			(0..3).each do |j|
				visited=[[false, false, false, false], [false, false, false, false], [false, false, false, false], [false, false, false, false]]
				solve_boggle(words, '', i, j, visited)
			end
		end

		words.uniq
	end

	def find_index(grid, letter)
		indexes = []
		grid.each_with_index { |value, i|
			value.each_with_index { |val, j|
				indexes << [i, j] if val == letter
			}
		}

		indexes
	end

	def generate_grid
		grid = []
		for i in (1..4)
			grid << letters[i*4 - 4...i*4].split("")
		end

		@grid = grid
	end

	def four_letter_word(seed)
		datamuse_url = "https://api.datamuse.com/words?sp="

		# select a 4 letter word from datamuse api
		# use seed for randomness
		response = RestClient.get(datamuse_url + seed + '??')

		# select words with highest score, they are very common and simple
		body_sample = JSON.parse(response.body).first(5)
		word = body_sample.sample
		last_two = true

		# there can be case when there is no 4 letters word with those initials
		# instead opt for letters that starts and ends
		if !word
			last_two = false
			response = RestClient.get(datamuse_url + [seed.first, '?', '?', seed.last].join(''))
			# select words with highest score, they are very common and simple
			body_sample = JSON.parse(response.body).first(5)
			word = body_sample.sample
		end

		[word["word"], last_two]
	end

	def better_sequence
		# select 2 random seed letters
		seed = [1, 2].map {|i| ('a'...'q').to_a.sample}.join('')

		word, last_two = four_letter_word(seed)
		letters_array = word.split('')

		while letters_array.length < 16
		seed = letters_array.slice(letters_array.length - 4 , 2).join('')
		word, last_two = four_letter_word(seed)
		letters_array += last_two ? word.last(2).split("") : [word[1], word[2]]
		end

		p "THE COMBINATION: #{letters_array.join('')}"

		letters_array.join('')
	end

end
