json.extract! boggle, :id, :created_at, :updated_at
json.url boggle_url(boggle, format: :json)
