class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def follow
    friend = User.find params[:friend_id]
    Friendship.create(following: friend, follower: current_user)

    redirect_to '/'
  end
end
