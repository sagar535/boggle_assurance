class BogglesController < ApplicationController
  before_action :set_boggle, only: [:show, :edit, :update, :destroy, :join, :finish]
  before_action :require_permission, only: [:show, :edit, :update, :destroy]
  before_action :require_user, only: :organize
  before_action :authenticate_user, only: [:index]

  include ActionView::Helpers::DateHelper

  # GET /boggles
  # GET /boggles.json
  def index
    @boggles = current_user.boggles.all
  end

  # GET /boggles/1
  # GET /boggles/1.json
  def show
    #render :edit
  end

  # GET /boggles/new
  def new
    @boggle = current_user.boggles.new if current_user.present?
    @boggle ||= Boggle.new
  end

  # GET /boggles/1/edit
  def edit
    set_match_id
  end

  # POST /boggles
  # POST /boggles.json
  def create
    @boggle = current_user.boggles.create(boggle_params) if current_user.present?
    @boggle ||= Boggle.new(boggle_params)

    respond_to do |format|
      if @boggle.save
        format.html {
          redirect_to edit_boggle_path(@boggle), notice: 'Boggle was successfully created.'
        }
        format.json { render :show, status: :created, location: @boggle }
      else
        format.html { render :new }
        format.json { render json: @boggle.errors, status: :unprocessable_entity }
      end
    end
  end

  def organize
    if current_user.organized_boggles.unfinished.present?
      flash[:alert] = 'Unfinished games are there, finish those before creating new'
      return redirect_to boggles_path
    end
    @boggle = current_user.organized_boggles.new

    respond_to do |format|
      if @boggle.save
        set_match_id if @boggle

        @boggle.boggle_players.create(player_id: current_user.id)
        format.html do
            ActionCable.server.broadcast 'boggle_game_channel',
                                         action: 'add-row',
                                         id:  @boggle.id,
                                         organizer: @boggle.organizer&.email || 'N/A',
                                         created_ago: time_ago_in_words(@boggle.created_at)

            redirect_back(fallback_location: root_path)
        end
      else
        format.html { render :new }
        format.json { render json: @boggle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /boggles/1
  # PATCH/PUT /boggles/1.json
  def update
    respond_to do |format|
      if @boggle.update(boggle_params)
        format.html { redirect_to @boggle, notice: 'Boggle was successfully updated.' }
        format.json { render :show, status: :ok, location: @boggle }
      else
        format.html { render :edit }
        format.json { render json: @boggle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /boggles/1
  # DELETE /boggles/1.json
  def destroy
    @boggle.destroy
    respond_to do |format|
      format.html { redirect_to boggles_url, notice: 'Boggle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def join
    @boggle.boggle_players.create(player_id: params[:player_id] || current_user.id )
    ActionCable.server.broadcast "match_channel_#{@boggle.id}",
                                 action: 'redirect-to',
                                 id:  @boggle.id

    respond_to do |f|
      f.js
      f.html { redirect_to edit_boggle_path(@boggle) }
    end
  end

  def finish
    if @boggle.organizer != current_user
      flash.now[:alert] = 'Only organizer can set finish to true'
    else
      flash.now[:notice] = 'Game on no more'
      @boggle.update(finished: true)

      ActionCable.server.broadcast 'boggle_game_channel',
                                   action: 'remove-row',
                                   id:  @boggle.id
    end
    redirect_to root_path
  end

  def solve_boggle
  end

  def solve
    @letters = params[:letters]
    if @letters.length != 16
      return redirect_to solve_boggle_path, alert: "Letters must be exactly 16 letters yours is #{@letters.length}"
    end
    boggle = Boggle.new(letters: @letters)
    @words = boggle.solve_for_all_position

    respond_to do |f|
      f.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_boggle
      @boggle = Boggle.find_by_id(params[:id]) || Boggle.find(params[:boggle_id])
    end

    # Only allow a list of trusted parameters through.
    def boggle_params
      params.require(:boggle).permit(:finished, :begined_at)
    end

    def require_permission
      boggle = Boggle.find(params[:id])
      begin
        # cases when a user an access boggle
        # when boggle user and organizer is nil
        return true if boggle.organizer.nil?
        # when boggle players list contains current user's id
        return true if boggle.boggle_players.pluck(:player_id).include?(current_user.id) || boggle.organizer_id == current_user.id
        redirect_to root_path if current_user != boggle.user
      rescue
        flash[:alert] = "Permission Denied."
        redirect_to root_path
      end
    end

    def require_user
      return true if current_user.present?

      flash[:notice] = 'Login to organize a game.'
      redirect_to root_path if !current_user.present?
    end

    def authenticate_user
      redirect_to root_path unless current_user.present?
    end

    def set_match_id
      cookies[:match_id] = @boggle.id
    end
end
