class SessionsController < Devise::SessionsController
  def omniauth
    @user = User.from_omniauth(auth)
    @user.save
    sign_in(:user, @user)
    redirect_to root_path
  end
  private
  def auth
    request.env['omniauth.auth']
  end
end
