class BoggleWordsController < ApplicationController
	before_action :set_boggle
	before_action :set_boggle_player, if: proc {current_user.present?}

	def create
		@boggle_word = @boggle.boggle_words.create(word: params[:word], boggle_player_id: @boggle_player&.id) unless @boggle.finished?

		if @boggle_player.present?
			ActionCable.server.broadcast "match_channel_#{@boggle.id}",
																	 action: 'update-score',
																	 user_id: current_user.id,
																	 score: @boggle_player.reload.score
		end

		return respond_to { |f| f.js } if @boggle_word.present?
		respond_to {|f| f.json { render json: {
				error: 'Boggle Word creation failed'
		} }}
	end

	private
	def set_boggle
		@boggle = Boggle.find params[:boggle_id]
	end

	def set_boggle_player
		@boggle_player = BogglePlayer.find_by(boggle_id: params[:boggle_id], player_id: current_user.id)
	end

    # Only allow a list of trusted parameters through.
    def boggle_word_params
      params.require(:boggle).permit(:word)
    end
end
