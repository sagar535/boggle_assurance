import consumer from "./consumer"

consumer.subscriptions.create("BoggleGameChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
    console.log("data", data)
    switch(data.action) {
      case 'add-row':
        $("#game-body").prepend("<tr id='boggle-row-"+ data.id +"'>" +
            "<td>"+ data.id +"</td>"+
            "<td>"+ data.organizer +"</td>"+
            "<td>"+ data.created_ago +"</td>"+
            "<td>"+
            "<button class='btn btn-primary join-button' id='join-button-" + data.id + `' data-boggle-id=${data.id}>JOIN</button>`+
            "</td>"+
            "</tr>")
            break
      case 'remove-row':
        $('#boggle-row-'+data.id).hide()
        break
      default:
          console.log('Action mismatch for ', data.action)
    }

  }
});
