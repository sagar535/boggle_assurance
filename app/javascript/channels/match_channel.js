import consumer from "./consumer"

consumer.subscriptions.create("MatchChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
    console.log("match channel data", data)
    switch (data.action) {
      case 'redirect-to':
        window.location.replace('/boggles/'+data.id+'/edit')
        break
      case 'update-score':
        $('#score-'+data.user_id).text('SCORE '+data.user_id +' : '+data.score)
        break
      default:
        console.log('action mismatch')
    }
  }
});
