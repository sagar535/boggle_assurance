$("#play-boggle").on('click', () => {
    $("#boggle_begined_at").val(Date.now())
    console.log("Starting new game")
    $("#new_boggle").submit()
})

$(document).ready(() => {
    $(document).on('click', '.join-button', (e) => {
        console.log("Aboout to join the game")
        console.log($(e.target).data('boggle-id'))

        const boggleId = $(e.target).data('boggle-id')
        $.ajax({
            type: "PATCH",
            url: `/boggles/${boggleId}/join`
        });
    })
})
