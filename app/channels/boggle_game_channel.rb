class BoggleGameChannel < ApplicationCable::Channel
  def subscribed
     # we will treat this channel as global game channel
     stream_from "boggle_game_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
