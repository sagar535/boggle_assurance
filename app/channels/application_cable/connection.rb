module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :match_id

    def connect
      self.match_id = get_match_id
    end

    private
    def get_match_id
      cookies[:match_id] || ''
    end
  end
end
