class AddCorrectFieldToBoggleWords < ActiveRecord::Migration[6.0]
  def change
  	add_column :boggle_words, :correct, :boolean
  end
end
