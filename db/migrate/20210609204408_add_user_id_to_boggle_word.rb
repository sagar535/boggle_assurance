class AddUserIdToBoggleWord < ActiveRecord::Migration[6.0]
  def change
    add_column :boggle_words, :user_id, :integer, index: true
  end
end
