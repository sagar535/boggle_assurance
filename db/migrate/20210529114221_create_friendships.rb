class CreateFriendships < ActiveRecord::Migration[6.0]
  def change
    create_table :friendships do |t|
      t.belongs_to :follower
      t.belongs_to :following

      t.timestamps
    end

    add_index :friendships, [:follower_id, :following_id], unique: true
  end
end
