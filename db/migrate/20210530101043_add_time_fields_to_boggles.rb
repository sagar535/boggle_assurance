class AddTimeFieldsToBoggles < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :begined_at, :datetime
    add_column :boggles, :playing_time, :integer, default: (5 * 60) # 5 mins time in seconds
  end
end
