class AddBogglePlayerIdToBoggleWord < ActiveRecord::Migration[6.0]
  def change
    add_column :boggle_words, :boggle_player_id, :integer
  end
end
