class AddScores < ActiveRecord::Migration[6.0]
  def change
  	add_column :boggles, :score, :integer, default: 0
  	add_column :boggle_words, :score, :integer, default: 0 
  end
end
