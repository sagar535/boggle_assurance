class CreateBoggleWords < ActiveRecord::Migration[6.0]
  def change
    create_table :boggle_words do |t|
      t.references :boggle, foreign_key: true
      t.string :word
      
      t.timestamps
    end
  end
end
