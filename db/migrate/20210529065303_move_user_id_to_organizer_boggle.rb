class MoveUserIdToOrganizerBoggle < ActiveRecord::Migration[6.0]
  def change
    Boggle.where.not(user: nil).each do |boggle|
      boggle.update_columns(organizer_id: boggle.user_id)
    end
  end
end
