class CreateBogglePlayers < ActiveRecord::Migration[6.0]
  def change
    create_table :boggle_players do |t|
      t.belongs_to :boggle
      t.belongs_to :player
      t.timestamps
    end
  end
end
