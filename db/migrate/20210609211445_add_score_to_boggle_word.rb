class AddScoreToBoggleWord < ActiveRecord::Migration[6.0]
  def change
    add_column :boggle_players, :score, :integer, default: 0
  end
end
