class AddFinisedToBoggle < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :finished, :boolean, default: false
  end
end
