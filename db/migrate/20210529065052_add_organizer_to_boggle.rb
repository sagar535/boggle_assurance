class AddOrganizerToBoggle < ActiveRecord::Migration[6.0]
  def change
    add_column :boggles, :organizer_id, :integer
  end
end
