class CreateBoggles < ActiveRecord::Migration[6.0]
  def change
    create_table :boggles do |t|
      t.string :letters
      
      t.timestamps
    end
  end
end
