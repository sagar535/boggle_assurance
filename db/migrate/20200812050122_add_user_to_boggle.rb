class AddUserToBoggle < ActiveRecord::Migration[6.0]
  def change
    add_reference :boggles, :user, index: true
  end
end
