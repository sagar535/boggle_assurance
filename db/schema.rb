# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_09_211445) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boggle_players", force: :cascade do |t|
    t.bigint "boggle_id"
    t.bigint "player_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "score", default: 0
    t.index ["boggle_id"], name: "index_boggle_players_on_boggle_id"
    t.index ["player_id"], name: "index_boggle_players_on_player_id"
  end

  create_table "boggle_words", force: :cascade do |t|
    t.bigint "boggle_id"
    t.string "word"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "correct"
    t.integer "score", default: 0
    t.integer "user_id"
    t.integer "boggle_player_id"
    t.index ["boggle_id"], name: "index_boggle_words_on_boggle_id"
  end

  create_table "boggles", force: :cascade do |t|
    t.string "letters"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "score", default: 0
    t.bigint "user_id"
    t.integer "organizer_id"
    t.boolean "finished", default: false
    t.datetime "begined_at"
    t.integer "playing_time", default: 300
    t.integer "match_id"
    t.index ["user_id"], name: "index_boggles_on_user_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "follower_id"
    t.bigint "following_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["follower_id", "following_id"], name: "index_friendships_on_follower_id_and_following_id", unique: true
    t.index ["follower_id"], name: "index_friendships_on_follower_id"
    t.index ["following_id"], name: "index_friendships_on_following_id"
  end

  create_table "matches", force: :cascade do |t|
    t.boolean "matched"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "matches_users", force: :cascade do |t|
    t.integer "match_id"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "boggle_words", "boggles"
end
