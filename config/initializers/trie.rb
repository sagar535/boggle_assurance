require 'trie'

class Trie
	def self.word_trie
		trie = Trie.new

		File.open("./app/assets/trie/words.txt", "r") do |f|
			f.each_line do |line|
				# p line
				trie.add line.strip.downcase
			end
		end

		trie
	end

	WORD_TRIE = word_trie
end
