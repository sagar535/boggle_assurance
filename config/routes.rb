Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get '/auth/google_oauth2/callback' => 'sessions#omniauth'
  end
  resources :boggles do
  	resources :boggle_words, only: [:create]
    patch '/join' => 'boggles#join'
    patch '/finish' => 'boggles#finish'
  end
  get '/users' => 'users#index'
  post '/follow' => 'users#follow'
  post '/organize' => 'boggles#organize'
  get '/solve_boggle' => 'boggles#solve_boggle'
  post '/solve' => 'boggles#solve'

  root to: "boggles#new"

  mount ActionCable.server, at: '/cable'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
